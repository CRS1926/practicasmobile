package com.example.practica1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {


    private lateinit var wlecomeMessageText: TextView;
    private lateinit var welcomeSubtitleTextView: TextView;
    private lateinit var changeTextButton: Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        wlecomeMessageText = findViewById(R.id.welcome_message);
        welcomeSubtitleTextView = findViewById(R.id.Welcome_uptitle);
        changeTextButton = findViewById(R.id.Cambiar_texto)

        changeTextButton.setOnClickListener{changeMessageAndSubtitle()}


    }

    private fun changeMessageAndSubtitle (){
        //welcomeSubtitleTextView.text = "Si cambio?";
        //wlecomeMessageText.text = "Sha se cambio prro";


        welcomeSubtitleTextView.text = getString(R.id.Welcome_uptitle);
        wlecomeMessageText.text = "Sha se cambio prro";
    }


}
